FROM php:7.3-apache
RUN apt-get update && apt-get install -y wget unzip nano ffmpeg exiftool libjpeg-progs libmagickwand-dev --no-install-recommends && rm -rf /var/lib/apt/lists/*
RUN printf "\n" | pecl install imagick
RUN docker-php-ext-enable imagick
RUN docker-php-ext-install mysqli mbstring exif gd
RUN wget -O piwigo.zip piwigo.org/download/dlcounter.php?code=2.9.5 && unzip piwigo.zip -d /var/www/html
RUN rm piwigo.zip
RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini