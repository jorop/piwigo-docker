# piwigo-docker

Piwigo in a docker container. Should work out of the box with docker-compose

**Clone with git**  
**Copy** `.env.example` to `.env` and open the `.env`-file  
**Edit** `.env`-file and give the variables appropriate values  
**Start with** `docker-compose up -d`  


## Install

After docker-compose started the containers, the web-frontend of Piwigo is available with `http://localhost:8000/piwigo`  

The Database form asks for **Host** -> enter `mysqlpiwigo` there. The rest as you like it (according to the variables in the `.env`-file)

After installation you get a message to copy the php-snippet into a file. Read the message and do as it says.

